/**
 * Created by Nikunj on 19-04-2016.
 */
// FILE: configs/connection.js
require('dotenv').config();
var mysqlAdapter = require('sails-mysql');
var myDiskAdapter = require('sails-disk');

module.exports = {

    // setup adapters for each type of database
    adapters: {
        'default': mysqlAdapter,
        disk: myDiskAdapter,
        mysql: mysqlAdapter
    },

    // setup all type of connections you could have
    connections: {
        // mysql connection configs
        mysqlLocalhost: {
            adapter: 'mysql',
            host: process.env.DB_HOST,
          //  port:'8889', // For Local
            port:'3306', // For Server
            database: process.env.DB_DATABASE, // change your database name here
            user: process.env.DB_USERNAME,
            password: process.env.DB_PASSWORD
        },
        //using sails-disk method
        diskLocal: {
            adapter: 'disk'
        }
    },

    // some config about migration or something
    defaults: {
        // migration mode
        migrate: 'safe'
        // migrate: 'alter'
    }
};
