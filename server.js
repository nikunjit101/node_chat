var express = require('express');
var passport = require('passport');
var Strategy = require('passport-local').Strategy;
var ensureLogin = require('connect-ensure-login').ensureLoggedIn();
var userDataManager = require('./dataManager/user_data_manager');
var controller = require('./controller/controller.js');
var pushNotification = require('./controller/pushNotification');
var orm = require('./ORM/orm.js');
var constants = require('./common/constants').constants;

var parser = require('./common/parser');
var custom = require('./common/app');

var crypto = require('crypto');

// var logger = require('./common/logger');

// Configure the local strategy for use by Passport.
//
// The local strategy require a `verify` function which receives the credentials
// (`username` and `password`) submitted by the user.  The function must verify
// that the password is correct and then invoke `cb` with a user object, which
// will be set at `req.user` in route handlers after authentication.
passport.use(new Strategy({
        passReqToCallback: true
    },
    function (req, username, password, cb) {
        var md5Pass = crypto.createHash('md5').update(password).digest("hex");
        userDataManager.findByUsername(req, username, function (err, user) {
            if (err) {
                return cb(err);
            }
            if (!user) {
                return cb(null, false, {message: 'Invalid username or password.'});
            }
            if (user.password != md5Pass) {
                return cb(null, false, {message: 'Invalid username or password.'});
            }
            return cb(null, user);
        });
    }));


// Configure Passport authenticated session persistence.
//
// In order to restore authentication state across HTTP requests, Passport needs
// to serialize users into and deserialize users out of the session.  The
// typical implementation of this is as simple as supplying the user ID when
// serializing, and querying the user record by ID from the database when
// deserializing.
passport.serializeUser(function (user, cb) {
    cb(null, user.id);
});

passport.deserializeUser(function (id, cb) {
    app.models.user.findOne({id: id})
        .exec(function (err, user) {
            if (err) {
                return cb(err);
            }
            cb(null, user);
        });
});


// Create a new Express application.
var app = express();
app.use(express.static("assets"));
var http = require('http').Server(app);
var io = require('socket.io')(http);


// Configure view engine to render EJS templates.
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

// Use application-level middleware for common functionality, including
// logging, parsing, and session handling.
app.use(require('morgan')('combined'));
app.use(require('cookie-parser')());
app.use(require('body-parser').urlencoded({extended: true}));

var path = require('path');
var formidable = require('formidable');
var fs = require('fs');

var session = require("express-session")({
    secret: "keyboard cat",
    resave: true,
    saveUninitialized: true
});
var sharedsession = require("express-socket.io-session");

// Attach session
app.use(session);

// Share session with io sockets
io.use(sharedsession(session));


// Initialize Passport and restore authentication state, if any, from the
// session.
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(__dirname));


// FILE: app.js, before any routing functions
// ORM middleware
app.use(function (req, res, next) {
    req.models = app.models;
    next();
});

// Define routes.
app.get('/',
    function (req, res) {
        if (req.user) {
            req.models.user.find().exec(function (err, allUsers) {
                if (err) return res.status(500).json({error: err});
                res.render('userChat', {
                    user: req.user,
                    allUsers: allUsers,
                    onlineUsers: userDataManager.getOnlineUsers(),
                    newUser: false
                });
            });
        } else {
            res.render('login', {user: req.user});
        }
    });

app.get('/login',
    function (req, res) {
        if (req.user) {
            req.models.user.find().exec(function (err, allUsers) {
                if (err) return res.status(500).json({error: err});
                res.render('userChat', {
                    user: req.user,
                    allUsers: allUsers,
                    onlineUsers: userDataManager.getOnlineUsers(),
                    newUser: false
                });
            });
        } else {
            res.render('login');
        }
    });


app.post('/login',
    function (req, res) {
        passport.authenticate('local', function (err, user, info) {
            if (err || !user) {
                // res.status(400).send({message: info.message, success: false, data: null});
                res.render('login',{
                    errMsg:info.message
                });
            }
            else {
                req.logIn(user, function (err) {
                    if (err) {
                        res.send(err);
                    }
                    req.models.user.find().exec(function (err, allUsers) {
                        if (err) return res.status(500).json({error: err});
                        res.render('userChat', {
                            user: req.user,
                            allUsers: allUsers,
                            onlineUsers: userDataManager.getOnlineUsers(),
                            newUser: false
                        });
                    });
                });
            }
        })(req, res);
    });

app.get('/registration',
    function (req, res) {
        if (req.user) {
            req.models.user.find().exec(function (err, allUsers) {
                if (err) return res.status(500).json({error: err});
                res.render('userChat', {
                    user: req.user,
                    allUsers: allUsers,
                    onlineUsers: userDataManager.getOnlineUsers(),
                    newUser: false
                });
            });
        } else {
            res.render('registration');
        }
    });

app.post('/registration',
    function (req, res) {
        var user = req.body;
        user['id'] = custom.makeId();
        user['password'] = crypto.createHash('md5').update(user['password']).digest("hex");
        req.models.user.create(user, function (err, createdUser) {
            if (err) return res.status(500).json({error: err});
            req.models.user.find().exec(function (err, allUsers) {
                if (err) return res.status(500).json({error: err});
                res.render('userChat', {
                    user: createdUser,
                    allUsers: allUsers,
                    onlineUsers: userDataManager.getOnlineUsers(),
                    newUser: true
                });
            });
        });
    });


app.get('/allUsers', function (req, res) {
    req.models.user.find().select(['username', 'email', 'phone', 'name', 'gender']).exec(function (err, allUsers) {
        if (err) return res.status(500).json({error: err});
        res.status(200).json({message: 'Retrieved Successfully', success: true, data: allUsers});
    });
});

app.get('/retrieveAllMessage', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    req.models.user_message.find({
        where: {
            or: [{
                reciever_id: req.query.reciever_id,
                sender_id: req.query.sender_id
            }, {
                sender_id: req.query.reciever_id,
                reciever_id: req.query.sender_id
            }]
        }, sort: 'sent_on ASC'
    }).exec(function (err, msgs) {
        if (err) return res.status(500).json({error: err});
        res.status(200).json({message: 'Retrieved Successfully', success: true, data: msgs});
    });
});
app.post('/updateBatchCountByUserId', function (req, res) {
    req.models.user.update({id: req.query.user_id}, {badge_count: 0}).exec(function (err, data) {
        if (err) return res.status(500).json({error: err});
        res.status(200).json({message: 'Retrieved Successfully', success: true});
    });
});

app.get('/pendingMessages', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    // if msg_id empty then all message retrieve
    if (req.query.msg_id == "") {
        req.models.user_message.find({
            where: {
                or: [{
                    reciever_id: req.query.reciever_id
                }, {
                    sender_id: req.query.reciever_id
                }]
            }, sort: 'sent_on ASC'
        }).exec(function (err, msgs) {
            if (err) return res.status(500).json({error: err});
            res.status(200).json({message: 'Retrieved Successfully', success: true, data: msgs});
        });
    } else {
        // else take sent_on from give msg_id and return all msg whose sent_on is greater than given msg_id
        req.models.user_message.findOne({id: req.query.msg_id})
            .exec(function (err, msg) {
                if (err) return res.status(500).json({error: err});
                if (!msg) return res.status(200).json({message: 'Message not found', success: true, data: []});
                req.models.user_message.find({
                    where: {
                        or: [{
                            reciever_id: req.query.reciever_id,
                            sent_on: {'>': msg.sent_on}
                        }, {
                            sender_id: req.query.reciever_id,
                            sent_on: {'>': msg.sent_on}
                        }]
                    }, sort: 'sent_on ASC'
                }).exec(function (err, msgs) {
                    if (err) return res.status(500).json({error: err});
                    res.status(200).json({message: 'Retrieved Successfully', success: true, data: msgs});
                });
            });
    }
});

app.get('/ackData', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    req.models.user_message.find({
        where: {
            sender_id: req.query.reciever_id,
            status: constants.DELIVERED,
            sent_on: {'>=': req.query.time}
        }, sort: 'sent_on ASC'
    }).select('id').exec(function (err, deliveredMsgs) {
        if (err) return res.status(500).json({error: err});

        req.models.user_message.find({
            where: {
                sender_id: req.query.reciever_id,
                status: constants.SEEN,
                sent_on: {'>=': req.query.time}
            }, sort: 'sent_on ASC'
        }).select(['id']).exec(function (err, seenMsgs) {
            if (err) return res.status(500).json({error: err});

            var objData = [{
                status: constants.DELIVERED,
                msg_ids: parser.parseAckData(deliveredMsgs)
            }, {
                status: constants.SEEN,
                msg_ids: parser.parseAckData(seenMsgs)
            }];
            res.status(200).json({message: 'Retrieved Successfully', success: true, data: objData});
        });
    });
});

app.post('/upload', function(req, res){

    // create an incoming form object
    var form = new formidable.IncomingForm();

    // specify that we want to allow the user to upload multiple files in a single request
    form.multiples = true;

    // store all uploads in the /uploads directory
    form.uploadDir = path.join(__dirname, '/uploads');

    // every time a file has been uploaded successfully,
    // rename it to it's orignal name
    form.on('file', function(field, file) {
        fs.rename(file.path, path.join(form.uploadDir, file.name));
    });

    // log any errors that occur
    form.on('error', function(err) {
        console.log('An error has occured: \n' + err);
    });

    // once all the files have been uploaded, send a response to the client
    // form.on('end', function() {
    //     res.end('success');
    // });

    // parse the incoming request containing the form data
    form.parse(req, function(err, fields, files) {
        // res.writeHead(200, {'content-type': 'text/plain'});
        // res.write('received upload:\n\n');
        res.end(custom.JSONEncode({files:files['uploads[]'],uploadDir:'/uploads'}));
    });

});

app.get('/onlineUsers', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    var onlineUsers = userDataManager.getOnlineUsersId();
    res.status(200).json({message: 'Retrieved Successfully', success: true, data: onlineUsers});
});

app.get('/logout',
    function (req, res) {
        req.logout();
        res.render('login');
    });

io.on('connection', function (socket) {
    var userId = socket.handshake.query.user_id; // For Mobile
    // var userId = socket.handshake.session.passport['user']; //For Desktop

    app.models.user.findOne({id: userId})
        .exec(function (err, user) {
            if (user) {
                if (!userDataManager.isUserOnline(userId))
                    socket.broadcast.emit('status_update', custom.JSONEncode({
                        userId: userId,
                        status: 1,
                        time: Date.now()
                    }));//onLine 1

                userDataManager.addOnlineUser(app.models, userId, socket.id);
                console.log(userId + ' user connected : ' + socket.id);


                socket.on('disconnect', function () {
                    userDataManager.removeOfflineUser(socket.id);
                    console.log(userId + ' user disconnected : ' + socket.id);

                    if (!userDataManager.isUserOnline(userId))
                        socket.broadcast.emit('status_update', custom.JSONEncode({
                            userId: userId,
                            status: 0,
                            time: Date.now()
                        }));//offline 0
                });

                socket.on('group_message', function (data) {
                    console.log('message: ' + data);
                    socket.broadcast.emit('group_message', data);
                });

                socket.on('user_message', function (data, callback) {
                    var clients = io.sockets.clients();
                    var onLineSocketUsers = userDataManager.getOnlineUserWithSockets();
                    // Save msg to database
                    controller.insertMessageData(app.models, data, onLineSocketUsers[socket.id].id, function (err, dbMsg) {

                        if (!err) {
                            onLineSocketUsers = userDataManager.getOnlineUserWithSockets();
                            // Send Ack To Sender
                            var sender = onLineSocketUsers[socket.id];
                            if (sender) {

                                callback(custom.JSONEncode({ack: constants.ACCEPTED, msg_ids: [data.id]}));
                                // socket.emit('ack', custom.JSONEncode({ack: constants.ACCEPTED, msg_id: data.id}));
                                controller.insertAcknowledgement(app.models, constants.ACCEPTED, sender.id, [data.id], function (err, response) {

                                });
                            }
                            else {
                                controller.insertAcknowledgement(app.models, constants.ACCEPTED, sender.id, [data.id], function (err, response) {

                                });
                            }

                            var isReceiverExist = userDataManager.isUserOnline(data.reciever_id);

                            if (isReceiverExist) {
                                for (var key in clients.sockets) {
                                    if (onLineSocketUsers[key].id == data.reciever_id) {
                                        console.log('From:' + onLineSocketUsers[socket.id].id + ' message: ' + data.message + ' To:' + data.reciever_id);
                                        clients.sockets[key].emit('user_message', custom.JSONEncode(data));
                                    }
                                }
                            }
                            else {

                                // Send Push to Receiver

                                app.models.user.findOne({id: data.reciever_id}).exec(function (userErr, user) {
                                    if (user) {
                                        app.models.user.findOne({id: sender.id}).exec(function (senderErr, sender) {
                                            if (sender) {
                                                // Increment badge count for Customer
                                                app.models.user.update({id: user.id}, {badge_count: user.badge_count + 1}).exec(function (custErr, updatedUser) {
                                                    if (updatedUser) {
                                                        app.models.user_tokens.find({
                                                            where: {
                                                                user_id: user.id
                                                            }
                                                        }).exec(function (tokenErr, tokens) {
                                                            if (tokenErr) {
                                                                console.log(tokenErr);
                                                            }
                                                            else {
                                                                for (var i = 0; i < tokens.length; i++) {
                                                                    pushNotification.sendNotificationToUser(tokens[i], custom.JSONEncode(data), data.msg_type, sender.username, dbMsg, updatedUser[0].badge_count);
                                                                }
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });

                                // Update Push time in msg table
                                controller.updatePushTime(app.models, data.id);
                            }
                        }
                    });
                });

                // Listen Acknowledgment
                socket.on('msg_ack', function (data, callback) {
                    // var data = custom.JSONDecode(data1);
                    var onLineSocketUsers = userDataManager.getOnlineUserWithSockets();
                    var clients = io.sockets.clients();
                    controller.insertAcknowledgement(app.models, data.ack, onLineSocketUsers[socket.id].id, data.msg_ids, function (error, response) {
                        if (!error) {
                            // Server Receive Ack
                            callback(data);
                            var msgId = data.msg_ids[0];

                            app.models.user_message.findOne({id: msgId}).exec(function (err, msg) {
                                if (err)
                                    console.log(err);

                                var isSenderOnline = userDataManager.isUserOnline(msg.sender_id);
                                if (isSenderOnline) {
                                    onLineSocketUsers = userDataManager.getOnlineUserWithSockets();
                                    for (var key in clients.sockets) {
                                        if (onLineSocketUsers[key].id == msg.sender_id) {
                                            console.log('Ack : From:' + data.reciever_id + ' message: ' + data.message + ' To:' + msg.sender_id);
                                            clients.sockets[key].emit('msg_ack', custom.JSONEncode({
                                                ack: data.ack,
                                                msg_ids: data.msg_ids
                                            }));
                                        }
                                    }
                                    // controller.insertAcknowledgement(app.models, data.ack, onLineSocketUsers[socket.id].user_id, data.msg_ids, function (response) {
                                    //
                                    // });
                                }
                                else {
                                    // Use Push
                                }
                            });
                        }
                    });
                });

                socket.on('new_reg_user_arrive', function (data) {
                    app.models.user.findOne({id: data.reg_user_id})
                        .exec(function (err, user) {
                            if (user) {
                                socket.broadcast.emit('new_reg_user_arrive', user);
                            }
                        });
                });
            }
            else {
                console.log('User not found!');
            }
        });
});

orm.initialize(app, 8080, function (collections, connections) {
    // store the collections and connections retrieved to the app instance
    app.models = collections;
    app.connections = connections;

    http.listen(8080, function () {
        console.log("Server Start");
        // logger.info('Server listening at port : %d', 8080);
    });
});
