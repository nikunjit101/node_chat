# Chatting App #

### Here is your process to work on the frontend ###

* `git clone git@bitbucket.org:devnikunj5538/node_chat.git`
* `cd node_chat`
* `npm install`
* Import db.sql in your local
* Setup .env file
* `node server.js`
* Now you can run https://localhost:8000
