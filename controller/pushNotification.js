/**
 * Created by Nikunj on 6/14/2016.
 */

require('dotenv').config();
var path = require('path');
var GCM = require('push-notify').gcm({
    "apiKey": "AIzaSyBWlrRqh2EwAL24PBQQc9EdKSi4YhF8dRg",
    "retries": 1
});

var APN = require('push-notify').apn({
    key: path.resolve('certificates/iOSKey.pem'),
    cert: path.resolve('certificates/iOSCertificate.pem'),
    passphrase: '123456',
    production: true
});

var constants = require('../common/constants').constants;
var custom = require('../common/app');

// Send notification to user
function sendNotificationToUser(tokenData, messageData, msgType, senderName, dbMsg, badgeCount) {
    if (tokenData.device_type == constants.DEVICE_TOKEN_TYPE_ANDROID) {
        sendNotificationToGCM(tokenData.token, messageData, msgType, senderName, dbMsg, badgeCount);
    }
    else if (tokenData.device_type == constants.DEVICE_TOKEN_TYPE_IOS) {
        sendNotificationToAPN(tokenData.token, messageData, msgType, senderName, dbMsg, badgeCount);
    }
}

// Send push notification to GCM user
function sendNotificationToGCM(regToken, data, msgType, senderName, dbMsg, badgeCount) {
    var msg = {
        message: JSON.parse(data).message,
        type: msgType,
        name: senderName,
        user_id: dbMsg.sender_id,
        badge: badgeCount
    };
    GCM.send({
        registrationId: regToken,
        delayWhileIdle: false,
        timeToLive: 3,
        data: {
            message: custom.JSONEncode(msg)
        }
    });

    GCM.on('transmitted', function (result, message, registrationId) {
        var response = {
            GCM: registrationId,
            result: result,
            message: message
        };
        console.log(response);
        console.log("GCM transmitted!");
    });

    GCM.on('transmissionError', function (error, message, registrationId) {
        var response = {
            GCM: registrationId,
            error: error,
            message: message
        };
        console.log(response);
        console.log("GCM not transmitted!");
    });

}

// Send notification to apn user
function sendNotificationToAPN(regToken, data, msgType, senderName, dbMsg, badgeCount) {

    var msg = {
        message: JSON.parse(data).message,
        type: msgType,
        name: senderName,
        user_id: dbMsg.sender_id
    };

    APN.send({
        token: regToken,
        alert: {
            body: JSON.parse(data).message,
            title: "Partsbay",
            data: {
                message: custom.JSONEncode(msg)
            }
        },
        badge: badgeCount
    });

    APN.on('transmitted', function (notification, device) {
        var response = {
            APN: device,
            result: notification
        };

        console.log(response);
        console.log("APN transmitted!");
    });

    APN.on('transmissionError', function (errorCode, notification, device) {
        var response = {
            APN: device,
            result: notification,
            error: errorCode
        };

        console.log(response);
        console.log("APN not transmitted!");
    });

    APN.on('error', function (error) {
        console.log("APN Error" + error)
    });

}

module.exports = {
    sendNotificationToUser: sendNotificationToUser
};