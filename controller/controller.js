/**
 * Created by Nikunj on 5/4/2016.
 */
var parser = require('../common/parser');
var constants = require('../common/constants').constants;
exports.insertMessageData = function (models, data, senderId, callback) {
    models.user_message.create({
            id: data.id,
            sender_id: senderId,
            reciever_id: data.reciever_id,
            msg_type: data.msg_type,
            message: data.message,
            status: constants.PENDING,
            sent_on: Date.now(),
            create_on: Date.now()
        })
        .exec(function (err, userMessage) {
            if (err)
                console.log(err);

            callback(err, userMessage)
        });
};

exports.insertAcknowledgement = function (models, ack, userId, msgIds, callback) {
    var data = parser.parseAcknowledgement(ack, userId, msgIds);
    models.acknowledgement.create(data).exec(function (err, acknowledgement) {
        if (err)
            console.log(err);
        // Update Message Status
        updateMessageStatus(models, msgIds, ack);
        callback(err, acknowledgement)
    });
};

exports.updatePushTime = function (models, msgId) {
    models.user_message.update({id: msgId}, {push_on: Date.now()})
        .exec(function (err, messageData) {
            if (err)
                console.log(err);
        });
};

function updateMessageStatus(models, msgIds, status) {
    var data = parser.parseMessageStatus(msgIds);
    if (status == constants.DELIVERED) {
        for (var i = 0; i < data.length; i++) {
            models.user_message.update(data[i], {status: constants.DELIVERED, delivered_on: Date.now()})
                .exec(function (err, userMessage) {
                    if (err)
                        console.log(err);
                });
        }
    }
    else if (status == constants.SEEN) {
        for (var i = 0; i < data.length; i++) {
            models.user_message.update(data[i], {status: constants.SEEN, seen_on: Date.now()})
                .exec(function (err, userMessage) {
                    if (err)
                        console.log(err);
                });
        }
    }
}

exports.getUserById = function (models, userId) {
    models.user.findOne({id: userId})
        .exec(function (err, user) {
            if (err)
                console.log(err);
            return user;
        });
};
