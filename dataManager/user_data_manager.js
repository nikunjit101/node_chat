var onLineUsers = {};
var onLineSocketUsers = {};

exports.findByUsername = function (req, username, cb) {
    req.models.user.findOne({username: username})
        .exec(function (err, user) {
            if (user)
                return cb(null, user);
            else
                return cb(null, null);
        });
};

exports.addOnlineUser = function (models, userId, socketId) {
    models.user.findOne({id: userId}).select(['username', 'email', 'mobile', 'name', 'gender'])
        .exec(function (err, user) {
            onLineUsers[userId] = user;
            onLineSocketUsers[socketId] = onLineUsers[userId];
            console.log(onLineSocketUsers);
        });
};

exports.removeOfflineUser = function (socketId) {
    if (onLineSocketUsers.hasOwnProperty(socketId)) {
        var offlineUserId = onLineSocketUsers[socketId].id;
        delete onLineSocketUsers[socketId];
        // console.log(onLineSocketUsers);
        checkUserIdAssociateWithOtherSocket(offlineUserId)
    }
};

function checkUserIdAssociateWithOtherSocket(userId) {
    var isExist = false;
    for (var key in onLineSocketUsers) {
        if (onLineSocketUsers[key].id == userId) {
            isExist = true;
            break;
        }
    }

    if (isExist == false) {
        delete onLineUsers[userId];
    }
}

exports.getOnlineUsers = function () {
    return onLineUsers;
};

exports.getOnlineUsersId = function () {
    var onlineUsersIdArr = [];
    for (var key in onLineUsers) {
        onlineUsersIdArr.push(key);
    }
    return onlineUsersIdArr;
};

exports.getOnlineUserWithSockets = function () {
    return onLineSocketUsers;
};

exports.isUserOnline = function (userId) {
    var isOnline = false;
    for (var key in onLineUsers) {
        if (key == userId) {
            isOnline = true;
            break;
        }
    }

    return isOnline;
};

