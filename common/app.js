/**
 * Created by Nikunj on 5/7/2016.
 */

function makeId() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function JSONEncode(data) {
    return JSON.stringify(data);
}

function JSONDecode(data) {
    return JSON.parse(data)
}

module.exports = {
    makeId: makeId,
    JSONEncode: JSONEncode,
    JSONDecode: JSONDecode
};