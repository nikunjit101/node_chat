/**
 * Created by Nikunj on 5/7/2016.
 */


var constants = require('../common/constants').constants;

/*
 *
 * Date Object Parse
 *
 * */

function parseTimestampToDate(timestamp) {
    var date = new Date(0);
    date.setUTCMilliseconds(timestamp);
    return date
}


/*
 *
 * User Message Parser
 *
 * */

function parseUserMessage(data) {

}


function parseAcknowledgement(ack, userId, msgIds) {
    var data = [];
    for (var i = 0; i < msgIds.length; i++) {
        data.push({
            ack: ack,
            user_id: userId,
            msg_id: msgIds[i]
        });
    }

    return data;
}


function parseMessageStatus(msgIds) {
    var data = [];
    for (var i = 0; i < msgIds.length; i++) {
        data.push({
            id: msgIds[i]
        });
    }
    return data;
}

function parseAckData(msgIds) {
    var data = [];
    for (var i = 0; i < msgIds.length; i++) {
        data.push(msgIds[i].id);
    }
    return data;
}

module.exports = {
    parseTimestampToDate: parseTimestampToDate,
    parseUserMessage: parseUserMessage,
    parseAcknowledgement: parseAcknowledgement,
    parseMessageStatus: parseMessageStatus,
    parseAckData: parseAckData
};
