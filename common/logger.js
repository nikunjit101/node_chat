/**
 * Created by Nikunj on 5/31/2016.
 */

var winston = require('winston');
var path = require('path');
winston.emitErrs = true;

var logger = new winston.Logger({
    levels: {
        info: 0,
        warn: 1,
        error: 2,
        verbose: 3,
        i: 4,
        db: 5
    },
    transports: [
        new winston.transports.File({
            filename: path.resolve('logs/logs.log'),
            handleExceptions: true,
            json: false,
            colorize: true,
            timestamp: function () {
                return new Date();
            },
            formatter: function (options) {
                // Return string will be passed to logger.
                return options.timestamp() +
                    ' : [' + options.level.toUpperCase() + '] : ' +
                    (undefined !== options.message ? options.message : '') +
                    (options.meta && Object.keys(options.meta).length ? '\n\t' +
                    JSON.stringify(options.meta) : '' );
            }
        })
    ],
    exitOnError: false
});

winston.addColors({
    info: 'green',
    warn: 'cyan',
    error: 'red',
    verbose: 'blue',
    i: 'gray',
    db: 'magenta'
});


module.exports = logger;
module.exports.stream = {
    write: function (message, encoding) {
        logger.info(message);
    }
};