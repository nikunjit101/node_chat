/**
 * Created by Nikunj on 5/5/2016.
 */
var constants = Object.freeze({

// User Messages
    PENDING: 1,
    DELIVERED: 2,
    SEEN: 3,

// Acknowledgement
    ACCEPTED: 1,
    DELIVERED: 2,
    READ: 3,

    DEVICE_TOKEN_TYPE_ANDROID: 'android',
    DEVICE_TOKEN_TYPE_IOS: 'ios',

//Message Type
    TEXT: 0,
    IMAGE: 1,
    VOICE: 2

});

module.exports = {
    constants: constants
};
