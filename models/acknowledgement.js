/**
 * Created by Nikunj on 19-04-2016.
 */
// FILE: models/acknowledgement.js.js
var Waterline = require('waterline');

module.exports = Waterline.Collection.extend({
    // tableName corresponds to database table name
    tableName: 'acknowledgement',

    // connection name corresponds to the connection we define in configs/connection.js
    connection: 'mysqlLocalhost',

    attributes: {
        ack: {
            type: 'integer',
            required: true
        },
        user_id: {
            type: 'string',
            required: true
        },
        msg_id: {
            type: 'string',
            required: true
        },


        // instance methods can be put here
        temporaryFunction: function () {
            // doing some stuff here
        }
    }
});
