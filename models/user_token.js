/**
 * Created by Nikunj on 10-10-2016.
 */
// FILE: models/user.js
var Waterline = require('waterline');

module.exports = Waterline.Collection.extend({
    // tableName corresponds to database table name
    tableName: 'user_tokens',

    // connection name corresponds to the connection we define in configs/connection.js
    connection: 'mysqlLocalhost',
    autoCreatedAt: false,
    autoUpdatedAt: false,

    attributes: {
        id: {
            type: 'integer',
            required: true,
            primaryKey: true,
            unique: true
        },
        user_id: {
            type: 'string',
            required: true
        },
        token: {
            type: 'string'
        },
        device_type: {
            type: 'string'
        },
        created_at: {
            type: 'float'
        },
        updated_at: {
            type: 'float'
        },

        // instance methods can be put here
        temporaryFunction: function () {
            // doing some stuff here
        }
    }
});