/**
 * Created by Nikunj on 19-04-2016.
 */
// FILE: models/user.js
var Waterline = require('waterline');

module.exports = Waterline.Collection.extend({
    // tableName corresponds to database table name
    tableName: 'user',

    // connection name corresponds to the connection we define in configs/connection.js
    connection: 'mysqlLocalhost',

    attributes: {
        id: {
            type: 'string',
            required: true,
            primaryKey: true,
            unique: true
        },
        name: {
            type: 'string',
            required: true,
            unique: true
        },
        username: {
            type: 'string',
            required: true,
            unique: true
        },
        gender: {
            type: 'integer',
            required: true
        },
        email: {
            type: 'email', // automatically got email validation
            required: true // required attribute
        },
        mobile: {
            type: 'string',
            required: true
        },
        password: {
            type: 'string',
            required: true
        },
        badge_count: {
            type: 'float',
            defaultsTo: 0
        },

        // instance methods can be put here
        temporaryFunction: function () {
            // doing some stuff here
        }
    }
});