/**
 * Created by Nikunj on 19-04-2016.
 */
// FILE: models/user_message.js
var Waterline = require('waterline');

module.exports = Waterline.Collection.extend({
    // tableName corresponds to database table name
    tableName: 'user_message',

    // connection name corresponds to the connection we define in configs/connection.js
    connection: 'mysqlLocalhost',

    attributes: {
        id: {
            type: 'string',
            required: true,
            primaryKey: true,
            unique: true
        },
        sender_id: {
            type: 'string'
        },
        reciever_id: {
            type: 'string'
        },
        msg_type: {
            type: 'integer',
            required: true
        },
        message: {
            type: 'text',
            required: true
        },
        status: {
            type: 'integer',
            required: true
        },
        sent_on: {
            type: 'float'
        },
        push_on: {
            type: 'float'
        },
        delivered_on: {
            type: 'float'
        },
        seen_on: {
            type: 'float'
        },
        create_on: {
            type: 'float'
        },

        // instance methods can be put here
        temporaryFunction: function () {
            // doing some stuff here
        }
    }
});