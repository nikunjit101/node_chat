/**
 * Created by Nikunj on 19-04-2016.
 */
// FILE: ORM/orm.js
var Waterline = require('waterline');

// create new instance of waterline
var orm = new Waterline();

// get connection configs
var connectionConfig = require('../configs/connection.js');

// load model definitions
var user = require('../models/user.js');
var userMessage = require('../models/user_message.js');
var acknowledgement = require('../models/acknowledgement.js');
var userToken = require('../models/user_token.js');

// load models into orm
orm.loadCollection(user);
// you can load more collections (models) here
orm.loadCollection(userMessage);
orm.loadCollection(acknowledgement);
orm.loadCollection(userToken);

// export an orm object
module.exports = {};

// initialize function
module.exports.initialize = function (app, PORT, callback) {
    // Initialize the whole database and store models and connections to app
    orm.initialize(connectionConfig, function (err, models) {
        if (err) throw err;

        // pass the collections (models) and connections created to app
        callback(models.collections, models.connections);

    });
};